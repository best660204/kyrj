#usr/bin/python
#coding utf-8

import time
import csv
from urllib.request import urlopen
from urllib.request import Request
import json


def get_results(search, headers, page):
    url = 'https://api.github.com/search/repositories?q={search}%20page={num}&per_page=100&sort=stars&order=desc'\
        .format(search=search, num=page)
    req = Request(url, headers=headers)
    response = urlopen(req).read()
    result = json.loads(response.decode())
    return result


if __name__ == '__main__':
    search = 'stars:>=100'  # 爬取star大于100的项目信息
    headers = {'User-Agent': 'Mozilla/5.0',
               'Authorization': 'token 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',
               'Content-Type': 'application/json',
               'Accept': 'application/json'
               }
    count = 1
    data_list = []  # 创建一个列表来保存所有的仓库信息
    for page in range(20):
        print("---page : {page}---".format(page=page))
        results = get_results(search, headers, page)
        for item in results['items']:
            data_list.append({'count': count, 'name': item["name"], 'clone_url': item["clone_url"],
                              'stars_count': item["stargazers_count"], 'forks_count': item["forks_count"],
                              'visibility': item["visibility"], 'language': item["language"],
                              'created_time': item["created_at"], 'size': item["size"],
                              'open_issues_count': item["open_issues_count"]})
            count += 1
    csv_file_path = "./dataset/data1.csv"
    with open(csv_file_path, mode='w', encoding='utf-8', newline='') as csv_file:
        fieldnames = ['count', 'name', 'clone_url', 'stars_count', 'forks_count', 'visibility', 'language',
                      'created_time', 'size', 'open_issues_count']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        # 写入表头
        writer.writeheader()
        # 写入数据
        for data in data_list:
            writer.writerow(data)

    print(f"CSV 文件已生成：{csv_file_path}")
