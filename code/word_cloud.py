#usr/bin/python
#coding utf-8

import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud,STOPWORDS

# 读取csv文件
df = pd.read_csv('C:/Users/杜添翼/Desktop/kyrj/dataset/data_description.csv')

# 将词汇转换为字符串
text = ' '.join(df['description'].tolist())

#自定义停用词
stopwords=STOPWORDS
stopwords.add('use,going,app,page,used,support,written,using,user')

# 创建词云对象
wordcloud = WordCloud(background_color='white', width=800, height=600,stopwords=stopwords).generate(text)


# 绘制词云图
plt.figure(figsize=(10, 8))
plt.imshow(wordcloud)
plt.axis('off')
plt.savefig("星数大于一百的项目描述词云图",dpi=600)
plt.show()
