#usr/bin/python
#coding utf-8

import csv
import pandas as pd
import seaborn as sns
import numpy as np
from matplotlib import pyplot as plt


# 将文件名存储在filename中
filename = 'C:/Users/杜添翼/Desktop/kyrj/dataset/data1.csv'
with open(filename) as f:
    data = pd.read_csv(f)
    #通过data阅读器生成df这个dataframe格式
    df = pd.DataFrame(data)
    #数据清洗，筛选出size>1000的行，size>1000的项目数量不多且影响折线图展现
    df1=df[df['size']<1000]
    sns.lineplot(y="stars_count",x="size",data=df1)
    plt.savefig("星数与项目size关系折线图,size小于1000下",dpi=600)
    plt.show()
    sns.lineplot(y="stars_count",x="forks_count",data=df)
    plt.savefig("星数与项目fork关系折线图",dpi=600)
    plt.show()
    sns.lineplot(y="stars_count",x="open_issues_count",data=df)
    plt.savefig("星数与项目打开次数关系折线图",dpi=600)
    plt.show()
    #将language列的类别和数量分别为x和y坐标绘制直方图
    sns.barplot(y=data['language'].value_counts().values,x=data['language'].value_counts().index)
    #保存文件到当前文件夹下并命名为“使用语言种类直方图”
    plt.savefig("使用语言种类直方图",dpi=1000)
    plt.show()
    #对data出现的统计结果（series类）进行清洗，x轴数据太多影响直方图观感 由series生成series类
    data_series=data['language'].value_counts()[data['language'].value_counts().values>10]
    sns.barplot(y=data_series.values,x=data_series.index)
    plt.savefig("使用语言种类直方图，语言数大于10下",dpi=600)
    plt.show()