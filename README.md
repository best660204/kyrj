# 开源软件基础大作业

## 小组成员

组长：杜添翼

组员：毛宏博、陈思奇、袁鼎

## 文件内容

### dataset

data1：爬取GitHub上星数大于100的数据

data_description：爬取GitHub上星数大于100的描述内容

### code

stars_over_100：爬取GitHub上星数大于100数据

data_visiable:爬取出的data1.csv文件进行数据清洗并进行可视化，统计使用语言，星数，项目fork数，项目size大小等等之间的关系

descriptions：爬取GitHub上星数大于100的描述内容

word_cloud:对data_description进行词云统计，在统计中我们屏蔽了一些常用词

### results

包含所有可视化生成的图片（折线图，直方图，词云图）